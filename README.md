# Juego de la Vida de Conway Extendido

## Descripción
Este notebook implementa una versión extendida del Juego de la Vida de Conway que incluye dos tipos diferentes de células vivas, A y B, con reglas distintas para su supervivencia y reproducción. El tablero del juego es generado y actualizado usando NumPy y Matplotlib para la visualización.

## Funciones

### `new_board(x, y, num_live_cells_A, num_live_cells_B, num_dead_cells)`
Crea un nuevo tablero con dimensiones especificadas y una distribución inicial de células.

### `get(board, x, y)`
Obtiene el valor de una celda en el tablero, con condiciones de contorno periódicas.

### `assign(board, x, y, value)`
Asigna un valor a una celda en el tablero, con condiciones de contorno periódicas.

### `count_neighbors(board, x, y)`
Cuenta el número de vecinos vivos de los tipos A y B para una célula dada.

### `process_life(board)`
Genera la siguiente iteración del tablero basada en el estado actual y las reglas del juego.

## Reglas del Juego

En esta versión extendida del Juego de la Vida, tenemos dos tipos de células: A y B. Cada tipo tiene sus propias reglas para supervivencia y reproducción, que interactúan de la siguiente manera:

- **Células tipo A:**
  - Una célula A sobrevive si tiene exactamente 2 o 3 vecinos del tipo A y no más de 2 vecinos del tipo B.
  - Una célula muerta se convierte en una célula A si tiene exactamente 3 vecinos A y no más de 2 vecinos del tipo B.
  - Una célula A muere si tiene más de 3 vecinos del tipo A, menos de 2 vecinos del tipo A o más de 2 vecinos del tipo B.

- **Células tipo B:**
  - Una célula B sobrevive si tiene exactamente 2 o 3 vecinos del tipo B y no más de 2 vecinos del tipo A.
  - Una célula muerta se convierte en una célula B si tiene exactamente 3 vecinos B y no más de 2 vecinos del tipo A.
  - Una célula B muere si tiene más de 3 vecinos del tipo B, menos de 2 vecinos del tipo B o más de 2 vecinos del tipo A.

Las reglas se aplican simultáneamente a cada célula en el tablero, creando la próxima generación del juego.

## Algoritmo de Metropolis

El notebook utiliza el algoritmo de Metropolis para simular la evolución de patrones basados en probabilidades condicionadas. Este algoritmo se usa para generar nuevas configuraciones del tablero en función de una función de probabilidad, que evalúa la probabilidad de transición entre estados.

### Implementación
- Se genera un par inicial de coordenadas al azar dentro de los límites del tablero.
- Se itera un número determinado de veces, generando en cada paso un nuevo par de coordenadas al azar.
- Se calcula la diferencia de la suma de logaritmos de las probabilidades de las nuevas coordenadas y las anteriores.
- Si esta diferencia es negativa, se acepta el nuevo estado; si es positiva, se acepta con una probabilidad basada en el cociente de las probabilidades de los estados nuevo y anterior.

Esta técnica permite explorar el espacio de configuraciones posibles de manera eficiente, proporcionando una herramienta poderosa para estudiar sistemas complejos como este juego extendido.

## Visualización
El notebook utiliza histogramas 2D y visualizaciones de imágenes para mostrar la distribución y evolución de las células en el tablero a lo largo del tiempo.

## Simulación
El notebook realiza simulaciones para explorar el comportamiento estadístico y la evolución de patrones en el tablero bajo condiciones iniciales aleatorias.

## Uso
Para ejecutar una simulación, simplemente ejecute todas las células del notebook. Ajuste los parámetros iniciales en la función `new_board` según sea necesario para explorar diferentes configuraciones y comportamientos del juego.

## Gráficos
En el directorio Plots se encuentran 2 gráficos extraidos del notebook.

- **Hist2D.png:** este gráfico corresponde al histograma dos dimensional que se obtiene a partir del número de células de la especie A con el número de células de la especie B una vez se ejecuta el juego de la vida extendido.
- **Metropolis2D.png:** este gráfico corresponde al histograma dos dimensional que se obtiene a partir de desarrollar el algoritmo de Metropolis.

Ambos gráficos se encuentran debidamente normalizados.